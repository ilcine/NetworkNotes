
## IP Öğrenme

```
host -t A google.com # ipv4
host -t AAAA google.com  # ipv6 

route -n -A inet6  # ipv6 routeleri görülür. or "route -n -A inet6"
route -n # ipv4 routeleri

ipv6calc --in ipv6 --out revnibbles.arpa 2a00:6d40:60:744d::77/64  # Benim kullandığım ipv6 nin arpa sını bulur; "sudo apt install ipv6calc" i kur. 

ping6 2001:4860:4860::8888  # Google ipv6  ping test
ping6 ipv6test.google.com # google ipv6 name ping test

ping6 2a00:6d40:60:744d::77  # my ipv6
ping6 www.ilcin.name.tr  # my ipv6

ifconfig # device ler listelenir.

```
