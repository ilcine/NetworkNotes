﻿ipv6_mask_prefix 

| Prefix | Address                | Network Address&              | Notes                                     |
|--------|------------------------|-------------------------------|-------------------------------------------| 
| /9     | 36,028,797,018,963,968 | /64&nbsp;(prefix)&nbsp;Network&nbsp;Address |       |
| /10    | 18,014,398,509,481,984 | /64 (prefix)  Network Address |       |
| /11    | 9,007,199,254,740,992  | /64 (prefix)  Network Address |       |
| /12    | 4,503,599,627,370,496  | /64 (prefix)  Network Address |       |
| /13    | 2,251,799,813,685,248  | /64 (prefix)  Network Address |       |
| /14    | 1,125,899,906,842,624  | /64 (prefix)  Network Address |       |
| /15    | 562,949,953,421,312    | /64 (prefix)  Network Address |       |
| /16    | 281,474,976,710,656    | /64 (prefix)  Network Address |       |
| /17    | 140,737,488,355,328    | /64 (prefix)  Network Address |       |
| /18    | 70,368,744,177,664     | /64 (prefix)  Network Address |       |
| /19    | 35,184,372,088,832 | /64 (prefix)  Network Address |        |
| /20 | 17,592,186,044,416 | /64 (prefix)  Network Address |        |
| /21 | 8,796,093,022,208 | /64 (prefix)  Network Address |        |
| /22 | 4,398,046,511,104 | /64 (prefix)  Network Address |        |
| /23 | 2,199,023,255,552 | /64 (prefix)  Network Address |        |
| /24 | 1,099,511,627,776 | /64 (prefix)  Network Address |        |
| /25 | 549,755,813,888 | /64 (prefix)  Network Address |        |
| /26 | 274,877,906,944 | /64 (prefix)  Network Address |        |
| /27 | 137,438,953,472 | /64 (prefix)  Network Address |        |
| /28 | 68,719,476,736 | /64 (prefix)  Network Address |        |
| /29 | 34,359,738,36 | /64 (prefix)  Network Address |        |
| /30 | 17,179,869,184 | /64 (prefix)  Network Address |        |
| /31 | 8,589,934,592 | /64 (prefix)  Network Address |        |
| /32 | 4,294,967,296 | /64 (prefix)  Network Address | example U.U. 2001:a98:|    
| /33 | 2,147,483,648 | /64 (prefix)  Network Address |        |
| /34 | 1,073,741,824 | /64 (prefix)  Network Address |        |
| /35 | 536,870,912 | /64 (prefix)  Network Address |        |
| /36 | 268,435,456 | /64 (prefix)  Network Address |        |
| /37 | 134,217,728 | /64 (prefix)  Network Address |        |
| /38 | 67,108,864 | /64 (prefix)  Network Address |        |
| /39 | 33,554,432 | /64 (prefix)  Network Address |        |
| /40 | 16,777,216 | /64 (prefix)  Network Address |        |
| /41 | 8,388,608 | /64 (prefix)  Network Address |        |
| /42 | 4,194,304 | /64 (prefix)  Network Address |        |
| /43 | 2,097,152 | /64 (prefix)  Network Address |        |
| /44 | 1,048,576 | /64 (prefix)  Network Address |        |
| /45 | 524,288 | /64 (prefix)  Network Address |        |
| /46 | 262,144 | /64 (prefix)  Network Address |        |
| /47 | 131,072 | /64 (prefix)  Network Address |        |
| /48 | 65,536 | /64 (prefix)  Network Address | Example U.U. 2001:a98:a010:| 
| /49 | 32,768 | /64 (prefix)  Network Address |        |
| /50 | 16,384 | /64 (prefix)  Network Address |        |
| /51 | 8,192 | /64 (prefix)  Network Address |        |
| /52 | 4,096 | /64 (prefix)  Network Address | Example U.U. 2001:a98:a010:X: (0 to f)| 
| /54 | 1,024 | /64 (prefix)  Network Address |        |
| /55 | 512 | /64 (prefix)  Network Address |        |
| /56 | 256 | /64 (prefix)  Network Address | Example U.U. 2001:a98:a010:XX: (00 to ff)|
| /57 | 128 | /64 (prefix)  Network Address |        |
| /58 | 64 | /64 (prefix)  Network Address |        |
| /59 | 32 | /64 (prefix)  Network Address |        |
| /60 | 16 | /64 (prefix)  Network Address | Example U.U. 2001:a98:a010:XXX: (000 to fff)|
| /61 | 8 | /64 (prefix)  Network Address |        |
| /62 | 4 | /64 (prefix)  Network Address |        |
| /63 | 2 | /64 (prefix)  Network Address |        |
| /64 | 18,446,744,073,709,551,616 | Addresses | Example U.U. 2001:a98:a010:XXXX (0000 to ffff)|
| /65 | 9,223,372,036,854,775,808 | Addresses |    | 
| /66 | 4,611,686,018,427,387,904 | Addresses |    | 
| /67 | 2,305,843,009,213,693,952 | Addresses |    | 
| /68 | 1,152,921,504,606,846,976 | Addresses |    | 
| /69 | 576,460,752,303,423,488 | Addresses |    | 
| /70 | 288,230,376,151,711,744 | Addresses |    | 
| /71 | 144,115,188,075,855,872 | Addresses |    | 
| /72 | 72,057,594,037,927,936 | Addresses |    | 
| /73 | 36,028,797,018,963,968 | Addresses |    | 
| /74 | 18,014,398,509,481,984 | Addresses |    | 
| /75 | 9,007,199,254,740,992 | Addresses |    | 
| /76 | 4,503,599,627,370,496 | Addresses |    | 
| /77 | 2,251,799,813,685,248 | Addresses |    | 
| /78 | 1,125,899,906,842,624 | Addresses |    | 
| /79 | 562,949,953,421,312 | Addresses |    | 
| /80 | 281,474,976,710,656 | Addresses |    | 
| /81 | 140,737,488,355,328 | Addresses |    | 
| /82 | 70,368,744,177,664 | Addresses |    | 
| /83 | 35,184,372,088,832 | Addresses |    | 
| /84 | 17,592,186,044,416 | Addresses |    | 
| /85 | 8,796,093,022,208 | Addresses |    | 
| /86 | 4,398,046,511,104 | Addresses |    | 
| /87 | 2,199,023,255,552 | Addresses |    | 
| /88 | 1,099,511,627,776 | Addresses |    | 
| /89 | 549,755,813,888 | Addresses |    | 
| /90 | 274,877,906,944 | Addresses |    | 
| /91 | 137,438,953,472 | Addresses |    | 
| /92 | 68,719,476,736 | Addresses |    | 
| /93 | 34,359,738,36 | Addresses |    | 
| /94 | 17,179,869,184 | Addresses |    | 
| /95 | 8,589,934,592 | Addresses |    | 
| /96 | 4,294,967,296 | Addresses |    | 
| /97 | 2,147,483,648 | Addresses |    | 
| /98 | 1,073,741,824 | Addresses |    | 
| /99 | 536,870,912 | Addresses |    | 
| /100 | 268,435,456 | Addresses |    | 
| /101 | 134,217,728 | Addresses |    | 
| /102 | 67,108,864 | Addresses |    | 
| /103 | 33,554,432 | Addresses |    | 
| /104 | 16,777,216 | Addresses |    | 
| /105 | 8,388,608 | Addresses |    | 
| /106 | 4,194,304 | Addresses |    | 
| /107 | 2,097,152 | Addresses |    | 
| /108 | 1,048,576 | Addresses |    | 
| /109 | 524,288 | Addresses |    | 
| /110 | 262,144 | Addresses |    | 
| /111 | 131,072 | Addresses |    | 
| /112 | 65,536 | Addresses |    | 
| /113 | 32,768 | Addresses |    | 
| /114 | 16,384 | Addresses |    | 
| /115 | 8,192 | Addresses |    | 
| /116 | 4,096 | Addresses |    | 
| /117 | 2,048 | Addresses |    | 
| /118 | 1,024 | Addresses |    | 
| /119 | 512 | Addresses |    | 
| /120 | 256 | Addresses |    | 
| /121 | 128 | Addresses |    | 
| /122 | 64 | Addresses |    | 
| /123 | 32 | Addresses |    | 
| /124 | 16 | Addresses |    | 
| /125 | 8 | Addresses |    | 
| /126 | 4 | Addresses |    | 
| /127 | 2 | Addresses |    | 
| /128 | 1 | Addresses |    |
